<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH."/third_party/Requests-1.7.0/library/Requests.php";

/*
valores para:
tamanho: [pequena, media, grande]
sabor: [calabresa, marguerita, portuguesa]
personalizacao: [extra_bacon, sem_cebola, borda_recheada]
*/

class Testslib {

    const WS_ENDPOINT = "http://localhost/ws-tests";
    const POST_REQUEST = 'post';
    const GET_REQUEST = 'get';
    const PUT_REQUEST = 'put';

    public function __construct() {
        Requests::register_autoloader();
    }

    /*REQUESTS*/
    private function resquest($url, $requestType, $options = array()){
        $headers = array();
        $response = Requests::{$requestType}(self::WS_ENDPOINT."/".$url, $headers, $options);
        if ($response->status_code == 200){
            $dados = json_decode($response->body);
            return $dados;  
        }else{
            return $response;
        }
    }  

    private function post($url, $options = array()){
        return $this->resquest($url, self::POST_REQUEST, $options);
    } 

    private function get($url, $options = array()){
        return $this->resquest($url, self::GET_REQUEST, $options);
    }   

    private function put($url, $options = array()){
        return $this->resquest($url, self::PUT_REQUEST, $options);
    }
    /*REQUESTS*/

    public function getPedido($id){
        $id = (int) $id;
        $response = $this->get('pizzas-uds/'.$id, array());
        echo "<pre>";
        print_r($response);
    }

    public function getPedidos(){
        $response = $this->get('pizzas-uds', array());
        echo "<pre>";
        print_r($response);
    }

    public function postPedidoComum(){
        $data['tamanho'] = 'pequena';
        $data['sabor'] = 'calabresa';
        $response = $this->post('pizzas-uds', $data);
        echo "<pre>";
        print_r($response);
    }

    public function postPedidoPersonalizado(){
        $data['tamanho'] = 'grande';
        $data['sabor'] = 'portuguesa';
        $data['personalizacao'] = 'borda_recheada';
        $response = $this->post('pizzas-uds', $data);
        echo "<pre>";
        print_r($response);
    }

    public function putPedido(){
        $data['id'] = 4; //id do pedido a ser alterado
        $data['tamanho'] = 'media';
        $data['sabor'] = 'marguerita';
        $data['personalizacao'] = 'borda_recheada';
        $response = $this->put('pizzas-uds', $data);
        echo "<pre>";
        print_r($response);
    }
}