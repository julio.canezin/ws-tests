<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_message extends Library{

	private $errorCode; 
	private $message;
	private $array;

	public function getErrorCode()
    {
        return $this->errorCode;
    }

    public function setErrorCode($errorCode)
    {
        $this->errorCode = $errorCode;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function setMessage($message)
    {
        $this->message = $message;
    }

	public function setErrorMessage($errorCode, $message){
		$this->setErrorCode($errorCode);
		$this->setMessage($message);
		$this->setArray();
    }

    private function setArray(){   	
        $this->array = array(
            'error' => $this->getErrorCode(),
            'message' => $this->getMessage(), 
        ); 
    }

    public function getArray()
    {
    	return $this->array;
    }

}
