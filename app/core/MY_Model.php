<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Model extends CI_Model{

	protected $table;  
	private $insertDate = 'data_cadastro';
	private $updateDate = 'data_alteracao';

    protected $dataCadastro;
    protected $dataAlteracao;

	public function __construct()
	{
		parent::__construct();
		$this->setTable($this->router->class);
	}

    public function getTable()
    {
        return $this->table;
    }

	public function setTable($table)
	{	
		$this->table = $table;
	}

	public function getInsertDate()
	{
		return $this->insertDate;
	}

	public function getUpdateDate()
	{
		return $this->updateDate;
	}

    public function getDataCadastro()
    {
        return $this->dataCadastro;
    }

    public function setDataCadastro($dataCadastro)
    {
        $this->dataCadastro = $dataCadastro;
    }

    public function getDataAlteracao()
    {
        return $this->dataAlteracao;
    }

    public function setDataAlteracao($dataAlteracao)
    {
        $this->dataAlteracao = $dataAlteracao;
    }

    public function get() 
    {
        return $this->db->get($this->getTable())->result();
    }

    public function getById($id, $table) 
    {
        return $this->db->where("id", $id)->get($table)->row();
    }

    public function insert($data = NULL) 
    {
    	$dataAdd[$this->getInsertDate()] = date('Y-m-d H:i:s');
        $dataAdd[$this->getUpdateDate()] = date('Y-m-d H:i:s');
        if (!empty($data))
        {
            $data = array_merge($dataAdd, $data);
        }
        else
        {
            $data = array_merge($dataAdd, $this->input->post());   
        }
        $this->db->insert($this->getTable(), $data);
        return $this->db->insert_id();
    }

    public function update($id, $data = NULL) 
    {
        $dataAdd[$this->getUpdateDate()] = date('Y-m-d H:i:s');
        if (!empty($data))
        {
            $data = array_merge($dataAdd, $data);
        }
        else
        {
            $data = array_merge($dataAdd, $this->input->post());   
        }
        $this->db->where('id', $id)->update($this->getTable(), $data);
        $error = $this->db->error();
        return empty($error['code']);
    }

    public function delete($id) 
    {
    	$this->db->delete($this->getTable(), array('id' => $id)); 
    	$error = $this->db->error();
        return empty($error['code']);
    }

    public function insertBatch($data)
    {
        $this->db->insert_batch($this->getTable(), $data);
        $error = $this->db->error();
        return empty($error['code']);
    }

    public function updateBatch($data, $column)
    {
        $this->db->update_batch($this->getTable(), $data, $column);
        $error = $this->db->error();
        return empty($error['code']);
    }

    public function getAll2Select()
    {
        $data = $this->get();
        $dataArray = array();
        foreach($data as $value)
        {
            $dataArray[$value->id] = $value->titulo;
        }
        return $dataArray;
    }
}
