<?php

class MY_Controller extends MX_Controller {

	const GET_METHOD = 'GET';
    const POST_METHOD = 'POST';
    const PUT_METHOD = 'PUT';
    const DELETE_METHOD = 'DELETE';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('base/base_model');
	}

	protected function sendData($data){
        header('Content-Type: application/json; charset=utf-8');
        $json = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $json;
        exit;
    }

    public function getRequestMethod(){
        return $this->input->server('REQUEST_METHOD');
    }

    public function sendError($errCode, $msg){
    	$this->api_message->setErrorMessage($errCode, $msg);
        $this->sendData($this->api_message->getArray());
    }

}
