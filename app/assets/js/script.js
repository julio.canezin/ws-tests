$(document).ready(function(){
	$.ajaxSetup({
    	type: 'POST',
		data: {
			csrf_test_name:CSRF_VALUE
		}
	});
});

function socialShare(share){
	if(share == "facebook"){
		window.open("https://www.facebook.com/dialog/share?app_id=140586622674265&display=popup&href="+window.location, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=300,left=600,width=600,height=400");
	}

	if(share == "twitter"){
		window.open("https://twitter.com/intent/tweet?text="+window.location+"&source=webclient", "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=50,left=600,width=600,height=270");
	}

	if(share == "google-plus"){
		window.open("https://plus.google.com/share?url="+window.location, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=300,left=600,width=600,height=400");
	}
}