$(document).ready(function(){

    /*MASKS*/
    $('.cep-mask').mask('99999-999');
    $('.cpf-mask').mask('999.999.999-99');
    $('.cnpj-mask').mask('99.999.999/9999-99');
    $('.date-mask').mask('99/99/9999');

    $('.phone-br-mask').focusout(function(){
        var phone, element;
        element = $(this);
        element.unmask();
        phone = element.val().replace(/\D/g, '');
        if(phone.length > 10) {
            element.mask("(99) 99999-999?9");
        } else {
            element.mask("(99) 9999-9999?9");
        }
    }).trigger('focusout');
    onlyNumbers();
});

function notEmptyHtml5(el)
{
    if (el.value == '') {
        el.setCustomValidity('Campo Obrigatório.');
    }
    else
    {
        el.setCustomValidity('');
    }
}

function onlyNumbers()
{
    $('.only-numbers').keyup(function() {
        $(this).val($(this).val().replace(/\D/, ''));
    });
}

function validateCPF(cpf) {  
    cpf = cpf.replace(/[^\d]+/g,'');    
    if(cpf == '') return false; 
    // Elimina CPFs invalidos conhecidos    
    if (cpf.length != 11 || 
        cpf == "00000000000" || 
        cpf == "11111111111" || 
        cpf == "22222222222" || 
        cpf == "33333333333" || 
        cpf == "44444444444" || 
        cpf == "55555555555" || 
        cpf == "66666666666" || 
        cpf == "77777777777" || 
        cpf == "88888888888" || 
        cpf == "99999999999")
            return false;       
    // Valida 1o digito 
    add = 0;    
    for (i=0; i < 9; i ++)       
        add += parseInt(cpf.charAt(i)) * (10 - i);  
        rev = 11 - (add % 11);  
        if (rev == 10 || rev == 11)     
            rev = 0;    
        if (rev != parseInt(cpf.charAt(9)))     
            return false;       
    // Valida 2o digito 
    add = 0;    
    for (i = 0; i < 10; i ++)        
        add += parseInt(cpf.charAt(i)) * (11 - i);  
    rev = 11 - (add % 11);  
    if (rev == 10 || rev == 11) 
        rev = 0;    
    if (rev != parseInt(cpf.charAt(10)))
        return false;       
    return true;   
}

function validateCNPJ(cnpj) {
 
    cnpj = cnpj.replace(/[^\d]+/g,'');
 
    if(cnpj == '') return false;
     
    if (cnpj.length != 14)
        return false;
 
    // Elimina CNPJs invalidos conhecidos
    if (cnpj == "00000000000000" || 
        cnpj == "11111111111111" || 
        cnpj == "22222222222222" || 
        cnpj == "33333333333333" || 
        cnpj == "44444444444444" || 
        cnpj == "55555555555555" || 
        cnpj == "66666666666666" || 
        cnpj == "77777777777777" || 
        cnpj == "88888888888888" || 
        cnpj == "99999999999999")
        return false;
         
    // Valida DVs
    tamanho = cnpj.length - 2
    numeros = cnpj.substring(0,tamanho);
    digitos = cnpj.substring(tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
      soma += numeros.charAt(tamanho - i) * pos--;
      if (pos < 2)
            pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(0))
        return false;
         
    tamanho = tamanho + 1;
    numeros = cnpj.substring(0,tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
      soma += numeros.charAt(tamanho - i) * pos--;
      if (pos < 2)
            pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(1))
          return false;
           
    return true;
    
}

function validateEmail(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
}

function isEmpty(valor){
    return valor.length == 0;
}

function showErrorMessage(msg){
    var html = new Array();
    html.push("<center>");
    html.push("<p><b>"+msg+"</b></p><br/>");
    html.push("<button onclick='cancelar();' title='Fechar' class='btn btn-lg btn-danger btn-block'>Fechar</button>");
    html.push("</center>");
    $(".fancybox").fancybox({
        'content': html.join(" "),
        'type': 'iframe',
        'width': 350,
        'height': 150,
        'autoSize': false,
        'autoDimensions': false
    }).trigger('click');
}

function validateURL(str) {
    var regex = /(http|https):\/\/(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/;
    return regex.test(str);
}

/*
    take a date in this format: 20/10/1987 and change to this format: 1987-10-20
*/
function dateReverse(date){
    var parts = date.split(/\//);
    parts.reverse();
    return parts.join('-');
}

function validateDateFormat(date) {
    return ((new Date(date)).toString() !== "Invalid Date") ? true : false;         
}


function validateOnlyLetters(str) {
    var regex = /^[0-9a-zA-Z_]+$/;
    return regex.test(str);
}

function getImageDefaultExtensions(){
    return ['.jpeg', '.jpg', '.gif', '.png'];
}

function getFileDefaultExtensions(){
    return ['.gif', '.png', '.pjpeg', '.jpeg', '.zip', '.rar', '.doc', '.docx', '.pdf'];
}

function hasExtension(inputID, exts) {
    var fileName = document.getElementById(inputID).value;
    return (new RegExp('(' + exts.join('|').replace(/\./g, '\\.') + ')$')).test(fileName);
}

function isURL(str) {
    var regex = /(http|https):\/\/(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/;
    return regex.test(str);
}