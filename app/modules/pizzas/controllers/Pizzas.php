<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pizzas extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('pizzas_model');
	}

	public function index($id = NULL) {
        $result = NULL;
        $rm = $this->getRequestMethod();
        if ($rm == self::GET_METHOD){
            if (!empty($id)){
                $result = $this->pizzas_model->getById($id, $this->pizzas_model->getTable());
            }else{
                $result = $this->pizzas_model->getAll();
            }
        }else if ($rm == self::POST_METHOD){
            $data = $this->input->post();
            $data = $this->pizzas_model->map($data);
            $valid = $this->pizzas_model->validate($data);
            if ($valid){
                $repeated = $this->pizzas_model->repeated($data);
                if (!$repeated){
                    $insertId = $this->pizzas_model->insert($data);
                    if (!empty($insertId)){
                        $result = $this->pizzas_model->getById($insertId, $this->pizzas_model->getTable());
                    }else{
                        $this->sendError('INSERT_PEDIDO_001', 'Ocorreu um erro ao salvar o pedido');
                    }
                }else{
                    $this->sendError('INSERT_PEDIDO_002', "Pedido já existe no banco de dados. Por favor, use o método PUT (ID:{$repeated->id})");
                }
            }
        }else if ($rm == self::PUT_METHOD){
            $put = file_get_contents('php://input');
            parse_str($put, $data);
            if (!empty($data['id'])){
                $data = $this->pizzas_model->map($data);
                $valid = $this->pizzas_model->validate($data);
                if ($valid){
                    $id = $this->pizzas_model->update($data['id'], $data);
                    if (!empty($id)){
                        $result = (object) $data;
                    }else{
                        $this->api_validation->sendError('UPDATE_PEDIDO_001', "Ocorreu um erro ao salvar o pedido");    
                    }
                }
            }else{
                $this->sendError('UPDATE_PEDIDO_002', "ID é obrigatório");
            }
        }else{
            $this->sendError('REQUEST_METHOD_001', 'Método de requisição não permitido');
        }  
        $this->sendData($result);
    }
}
