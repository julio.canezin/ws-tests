<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pizzas_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		$this->setTable('table_pedidos_pizzas');
	}

	public function getTable()
    {
        return $this->table;
    }

    public function setTable($table)
    {
        $this->table = $table;
    }

	public function getAll(){
		$result = $this->db->get($this->getTable())->result();
		if (!empty($result)){
			foreach ($result as &$value){
				$this->filterValues($value);
			}
		}
		return $result;
	}

	private function filterValues($value){}

	public function validate($data){

		if (empty($data['tamanho']))
		{
			Modules::run('base/sendError', 'ERR_PEDIDO_001', 'Tamanho é obrigatório');
		}
		else
		{
			if (!$this->api_validation->validateTamanhos($data['tamanho']))
			{
				Modules::run('base/sendError', 'ERR_PEDIDO_002', 'Tamanho não é válido');
			}
		}

		if (empty($data['sabor']))
		{
			Modules::run('base/sendError', 'ERR_PEDIDO_003', 'Sabor é obrigatório');
		}
		else
		{
			if (!$this->api_validation->validateSabores($data['sabor']))
			{
				Modules::run('base/sendError', 'ERR_PEDIDO_004', 'Sabor não é válido');
			}
		}

		if (!empty($data['personalizacao']))
		{
			if (!$this->api_validation->validatePersonalizacoes($data['personalizacao']))
			{
				Modules::run('base/sendError', 'ERR_PEDIDO_005', 'Personalização não é válida');
			}
		}

		return TRUE;

	}

	public function map($data){

		$buffer = $data;

		$buffer['tempo_preparo'] = 0;
		if ($data['tamanho'] == Api_validation::T_PEQUENA)
		{
			$buffer['total'] = 20.00;
			$buffer['tempo_preparo'] = 15;
		}
		else if ($data['tamanho'] == Api_validation::T_MEDIA)
		{
			$buffer['total'] = 30.00;
			$buffer['tempo_preparo'] = 20;	
		}
		else
		{
			$buffer['total'] = 40.00;
			$buffer['tempo_preparo'] = 25;		
		}

		if ($data['sabor'] == Api_validation::S_PORTUGUESA)
		{
			$buffer['tempo_preparo'] += 5;	
		}

		$buffer['personalizacao'] = '';
		$buffer['total_personalizacao'] = 0.00;
		$buffer['tempo_personalizacao'] = 0;
		if (!empty($data['personalizacao']))
		{
			if ($data['personalizacao'] == Api_validation::P_EXTRA_BACON)
			{
				$buffer['total_personalizacao'] = 3.00;
				$buffer['total'] += 3.00;
			}
			else if ($data['personalizacao'] == Api_validation::P_BORDA_RECHEADA)
			{
				$buffer['total_personalizacao'] = 5.00;
				$buffer['total'] += 5.00;
				$buffer['tempo_personalizacao'] = 5;
				$buffer['tempo_preparo'] += 5;
			}
			$buffer['personalizacao'] = $data['personalizacao'];
		}
		return $buffer;
	}

	public function repeated($data){
		$rPedido = $this->getByPizza($data);
		if (!empty($rPedido)){
			return $rPedido;
		}
	}

	private function getByPizza($data){
		$tamanho = (!empty($data['tamanho'])) ? $data['tamanho'] : ''; 
		$sabor = (!empty($data['sabor'])) ? $data['sabor'] : '';
		return $this->db->where('tamanho', $tamanho)->where('sabor', $sabor)->get($this->getTable())->row();
	}

}