<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tests extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function getPedidos() {
        $this->testslib->getPedidos();    
    }

    public function getPedido($id) {
        $this->testslib->getPedido($id);    
    }

    public function postPedidoComum() {
        $this->testslib->postPedidoComum();    
    }

    public function postPedidoPersonalizado() {
        $this->testslib->postPedidoPersonalizado();    
    }

    public function putPedido($id) {
        $this->testslib->putPedido($id);    
    }

}
