<meta charset="utf-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<?php if (!empty($seoInfoWebsite)){ ?>
	<title><?php echo $seoInfoWebsite->seo_titulo ?></title>
	<meta name="description" content="<?php echo $seoInfoWebsite->seo_descricao ?>" />
	<meta name="keyphrases" content="<?php echo $seoInfoWebsite->seo_palavras_chave ?>"/>
	<?php echo html_entity_decode($seoInfoWebsite->seo_html) ?>
<?php } ?>

<?php echo $seoGeneralTags ?>

<?php if (!empty($seoInfoWebsite)){ ?>
	<meta property="og:url" content="<?php echo $seoUrl?>"/>
	<meta property="og:title" content="<?php echo $seoInfoWebsite->seo_titulo?>"/>
	<meta property="og:site_name" content="<?php echo base_url()?>"/>
	<meta property="og:description" content="<?php echo $seoInfoWebsite->seo_descricao ?>"/>
	<?php if (!empty($seoInfoWebsite->seo_imagem)){ ?>
		<?php $extesion = pathinfo($seoInfoWebsite->seo_imagem, PATHINFO_EXTENSION); ?>
		<meta property="og:image" content="<?php echo base_url("admin").'/'.$seoInfoWebsite->seo_imagem; ?>" />
		<meta property="og:image:type" content="image/<?php echo $extesion ?>">
		<meta property="og:image:width" content="600">
		<meta property="og:image:height" content="315">
	<?php } ?>
	<meta property="og:type" content="website" />
<?php } ?>

<!-- Google Fonts -->
<!--AQUI-->

<!-- Css -->
<link rel="stylesheet" href="<?php echo base_url('website/assets/css/style.css') ?>" />
<link rel="stylesheet" href="<?php echo base_url('website/assets/css/responsive.css') ?>" />

<script src="https://code.jquery.com/jquery-3.2.1.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.js"></script>
<script src="<?php echo base_url('website/assets/js/plugins/maskedinput/jquery.maskedinput.js') ?>" type="text/javascript" language="javascript"></script>
<script src="<?php echo base_url('website/assets/js/script.js') ?>" type="text/javascript" language="javascript"></script>
<script src="<?php echo base_url('website/assets/js/validation.js') ?>" type="text/javascript" language="javascript"></script>
<script src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyBKyVKa9SzA3p481HNLSw-E5lGfNP_dhXs"></script>

<!-- Favicons -->
<link rel="shortcut icon" href="<?=base_url("website/assets/images")?>/favicon.ico">