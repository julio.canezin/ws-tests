<meta name="url" content="<?php echo $storeDomain ?>" />
<meta name="subject" content="Consumer Products and shopping/Online Store" />
<meta name="rating" content="General" />
<meta name="expires" content="never" />
<meta name="charset" content="ISO-8859-1" />
<meta name="distribution" content="Global" />
<meta name="language" content="portuguese, BR" />
<meta name="country" content="BRA" />
<meta name="currency" content="R$" />
<meta name="geo.placename" content="<?php echo $storeAddress ?>, <?php echo $storeNumber?> <?php echo $storeNeighbourhood ?> - CEP: <?php echo $storeZipcode ?>" />
<link rel="canonical" href="<?php echo $currentUrl ?>" />
<meta name="Msnbot" content="index,follow" />
<meta name="Inktomislurp" content="index,follow" />
<meta name="Unknownrobot" content="index,follow" />
<meta name="publisher" content="<?php echo $storeDomain ?>" />
<meta name="copyright" content="Copyright ©<?=date('Y')?>" /></textarea>