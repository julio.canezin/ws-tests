<div style="width: 440px;position: relative;left: 50%;margin-left: -220px">
    <?php include_once('pieces/header.php') ?>
    <div style="width:100%">
        <h3 style="font-family:'Trebuchet MS', Helvetica, sans-serif;font-size: 18px;color: #333333;">Chegou um contato!</h3>
        <p style="font-family:'Trebuchet MS', Helvetica, sans-serif;font-size: 14px;color: #6C6C6C;line-height: 18px;">Veja abaixo as informações que chegaram do website:</p>

        <p style="font-family:'Trebuchet MS', Helvetica, sans-serif;font-size: 14px;margin-bottom: 0px;padding-bottom: 0px;"><b>Nome</b></p>
        <p style="font-family:'Trebuchet MS', Helvetica, sans-serif;color: #6C6C6C;font-size: 14px;margin-top: 0px;padding-top: 4px;">{nome}</p>

        <p style="font-family:'Trebuchet MS', Helvetica, sans-serif;font-size: 14px;margin-bottom: 0px;padding-bottom: 0px;"><b>E-mail</b></p>
        <p style="font-family:'Trebuchet MS', Helvetica, sans-serif;color: #6C6C6C;font-size: 14px;margin-top: 0px;padding-top: 4px;">{email}</p>

        <p style="font-family:'Trebuchet MS', Helvetica, sans-serif;font-size: 14px;margin-bottom: 0px;padding-bottom: 0px;"><b>Mensagem</b></p>
        <p style="font-family:'Trebuchet MS', Helvetica, sans-serif;color: #6C6C6C;font-size: 14px;margin-top: 0px;padding-top: 4px;line-height:18px;">{mensagem}</p>
    </div>
    <?php include_once('pieces/footer.php') ?>
</div>