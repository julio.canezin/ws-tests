<?php

function convertToSelect($data, $key, $value)
{
	$buffer = array();
	$selectedKey = NULL;
	$selectedValue = NULL;
	foreach($data as $v)
	{
		$objVars = get_object_vars($v);
		if (array_key_exists($key, $objVars))
		{
			$selectedKey = $objVars[$key];
		}
		if (array_key_exists($value, $objVars))
		{
			$selectedValue = $objVars[$value];
		}
		$buffer[$selectedKey] = $selectedValue;
	}

	return $buffer;

}

function createFormMapping($data, $dataMapping)
{
	$arrayData = array();
	if (!empty($data))
	{
		foreach($dataMapping as $value)
		{
			$arrayData[$value] = $data->$value;
		}
	}
	else
	{
		foreach($dataMapping as $value)
		{
			$arrayData[$value] = NULL;
		}
	}
	return $arrayData;
}