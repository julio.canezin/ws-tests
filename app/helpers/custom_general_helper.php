<?php

function generateHash($tamanho = 10)
{
	$maiusculas = TRUE;
	$numeros = TRUE;
	$simbolos = TRUE;
	// Caracteres de cada tipo
	$lmin = 'abcdefghijklmnopqrstuvwxyz';
	$lmai = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$num = '1234567890';
	$simb = '!@#$%*-';
	// Variáveis internas
	$retorno = '';
	$caracteres = '';
	// Agrupamos todos os caracteres que poderão ser utilizados
	$caracteres .= $lmin;
	if ($maiusculas) $caracteres .= $lmai;
	if ($numeros) $caracteres .= $num;
	if ($simbolos) $caracteres .= $simb;
	// Calculamos o total de caracteres possíveis
	$len = strlen($caracteres);
	for ($n = 1; $n <= $tamanho; $n++) {
		// Criamos um número aleatório de 1 até $len para pegar um dos caracteres
		$rand = mt_rand(1, $len);
		// Concatenamos um dos caracteres na variável $retorno
		$retorno .= $caracteres[$rand-1];
	}
	return $retorno;
}

function getArrayAttr($data, $attr)
{
	$buffer = NULL;
	if (!empty($data))
	{
		foreach($data as $value)
		{
			$buffer[] = $value->$attr;
		}
	}
	return $buffer;
}

function customRedirect($path, $id = NULL, $param = NULL)
{
	Message::flush();
	if (!empty($id))
	{
		redirect($path.'/'.$id, $param);
	}
	else
	{
		redirect($path, $param);
	}
}

function removeFileOnDelete($id, $table, $field, $removeImageLabel = 'remover_imagem')
{
	$CI =& get_instance();
	$data = $CI->base_model->getById($id, $table);
	if (!empty($data))
	{
		if (empty($data->$field)) return TRUE;
		$file = APPPATH.$data->$field;
		if (file_exists($file))
		{
			$removed = unlink($file);
			if ($removed)
			{
				$_POST[$field] = '';
				unset($_POST[$removeImageLabel]);
			}
			return $removed;
		}
	}
}

function removeGalleryFileOnDelete($id, $table, $field)
{
	$CI =& get_instance();
	$CI->load->model('gallery/gallery_model');
	$galleryTable = $CI->gallery_model->getTable();
	$data = $CI->gallery_model->getGallery($id, $table);
	if (!empty($data))
	{
		foreach ($data as $value){
			if (empty($value->$field)) return TRUE;
			$file = APPPATH.$value->$field;
			if (file_exists($file))
			{
				$removed = $CI->gallerylib->unlinkImg($value->$field);
				if ($removed)
				{
					$_POST[$field] = '';
					unset($_POST['remover_imagem']);
				}
				return $removed;
			}
		}
	}
}

function imageConfig($column, $allowedTypes, $table, $dimensions, $path = NULL)
{
	return array(
		'allowed_types' => $allowedTypes,
		'table' => $table,
		'name' => $column,
		'max_width' => $dimensions['width'],
		'min_width' => $dimensions['width'],
		'max_height' => $dimensions['height'],
		'min_height' => $dimensions['height'],
		'path' => (!empty($path)) ? $path : '',
	);
}

function urlExists($url) {
    $headers = @get_headers($url);
    return substr($headers[0], 9, 3) == 200;
}

function decimal2BRCurrency($number)
{
	return 'R$ '.number_format($number, 2, ',', '');
}

function joinArrays($array1, $array2)
{
	if (!is_array($array1)) return FALSE;
	if (!is_array($array2)) return FALSE;
	$buffer = $array1;
	foreach($array2 as $value)
	{
		$buffer[] = $value;
	}
	return $buffer;
}

function text2urlReadable($text){
    return url_title(strtolower(convert_accented_characters($text)));
}

function getDomain()
{
    $CI =& get_instance();
    return preg_replace("/^[\w]{2,6}:\/\/([\w\d\.\-]+).*$/","$1", $CI->config->slash_item('base_url'));
}

function limitText($text, $limit, $suffix = '[...]')
{
	$buffer = NULL;
	if (strlen($text) > $limit)
	{
		$buffer = substr($text, 0, $limit).$suffix;
	}
	else
	{
		$buffer = $text;
	}
	return $buffer;
}