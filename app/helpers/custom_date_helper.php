<?php

function dateDb2Form($date){
	if (!empty($date)){
		$dbDate = explode('-', $date);
		return $dbDate[2].'/'.$dbDate[1].'/'.$dbDate[0];
	}
}

function dateForm2Db($date, $addCurrentHour = FALSE){
	if (!empty($date)){
		$dbDate = explode('/', $date);
		$hour = (!empty($addCurrentHour)) ? ' '.date('H:i:s') : '';
		return $dbDate[2].'-'.$dbDate[1].'-'.$dbDate[0].$hour;
	}
}

function dateGraterThan($dateInit, $dateEnd)
{
	$init = strtotime($dateInit);
	$end = strtotime($dateEnd);	
	return $init >= $end;
}