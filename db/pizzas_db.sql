-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: 02-Nov-2018 às 16:29
-- Versão do servidor: 5.7.24-0ubuntu0.18.04.1
-- PHP Version: 7.2.11-3+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pizzas_db`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(128) CHARACTER SET latin1 NOT NULL,
  `ip_address` varchar(45) CHARACTER SET latin1 NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Estrutura da tabela `table_pedidos_pizzas`
--

CREATE TABLE `table_pedidos_pizzas` (
  `id` int(11) NOT NULL,
  `tamanho` varchar(255) NOT NULL,
  `sabor` varchar(255) NOT NULL,
  `personalizacao` varchar(255) NOT NULL,
  `total_personalizacao` decimal(10,2) DEFAULT '0.00',
  `total` decimal(10,2) NOT NULL,
  `tempo_personalizacao` int(11) DEFAULT '0',
  `tempo_preparo` int(11) NOT NULL,
  `data_cadastro` datetime NOT NULL,
  `data_alteracao` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `table_pedidos_pizzas`
--

INSERT INTO `table_pedidos_pizzas` (`id`, `tamanho`, `sabor`, `personalizacao`, `total_personalizacao`, `total`, `tempo_personalizacao`, `tempo_preparo`, `data_cadastro`, `data_alteracao`) VALUES
(2, 'pequena', 'calabresa', '', '0.00', '20.00', 0, 15, '2018-11-02 00:32:50', '2018-11-02 00:32:50');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `table_pedidos_pizzas`
--
ALTER TABLE `table_pedidos_pizzas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `table_pedidos_pizzas`
--
ALTER TABLE `table_pedidos_pizzas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
