###################
What is CodeIgniter
###################

CodeIgniter is an Application Development Framework - a toolkit - for people
who build web sites using PHP. Its goal is to enable you to develop projects
much faster than you could if you were writing code from scratch, by providing
a rich set of libraries for commonly needed tasks, as well as a simple
interface and logical structure to access these libraries. CodeIgniter lets
you creatively focus on your project by minimizing the amount of code needed
for a given task.

*******************
Release Information
*******************

This repo contains in-development code for future releases. To download the
latest stable release please visit the `CodeIgniter Downloads
<https://codeigniter.com/download>`_ page.

**************************
Changelog and New Features
**************************

You can find a list of all changes for each release in the `user
guide change log <https://github.com/bcit-ci/CodeIgniter/blob/develop/user_guide_src/source/changelog.rst>`_.

*******************
Server Requirements
*******************

PHP version 5.6 or newer is recommended.

It should work on 5.3.7 as well, but we strongly advise you NOT to run
such old versions of PHP, because of potential security and performance
issues, as well as missing features.

************
Installation
************

Please see the `installation section <https://codeigniter.com/user_guide/installation/index.html>`_
of the CodeIgniter User Guide.

*******
License
*******

Please see the `license
agreement <https://github.com/bcit-ci/CodeIgniter/blob/develop/user_guide_src/source/license.rst>`_.

*********
Resources
*********

-  `User Guide <https://codeigniter.com/docs>`_
-  `Language File Translations <https://github.com/bcit-ci/codeigniter3-translations>`_
-  `Community Forums <http://forum.codeigniter.com/>`_
-  `Community Wiki <https://github.com/bcit-ci/CodeIgniter/wiki>`_
-  `Community IRC <https://webchat.freenode.net/?channels=%23codeigniter>`_

Report security issues to our `Security Panel <mailto:security@codeigniter.com>`_
or via our `page on HackerOne <https://hackerone.com/codeigniter>`_, thank you.

***************
Acknowledgement
***************

The CodeIgniter team would like to thank EllisLab, all the
contributors to the CodeIgniter project and you, the CodeIgniter user.

***************
EXERCÍCIO
***************

Peculiaridades do projeto:

- Caso a url base do projeto mude de http://localhost/ws-tests/ para outra qualquer, esta url deve ser mudada no arquivo ws-tests/config/system_constants.php;
- Banco de dados: o dump do banco de dados pizzas_db.sql referente ao exercício se localiza no diretório: ws-tests/db;
- A configuração do banco de dados está no arquivo db.php no diretório ws-tests/config;

Com relação ao exercício:

1) 	Chamadas POST, GET e PUT poderão ser realizadas ao endpoint: "http://localhost/ws-tests". Foi escrita uma biblioteca de testes localizada em ws-tests/app/libraries/Testlib.php. Neste arquivo o endpoint poderá ser mudado conforme desejado na constante "WS_ENDPOINT". A url não possui barra no final, diferentemente dos itens acima que é o caso da url base dada de exemplo.

2) 	A biblioteca Testslib.php (acima) trabalha em conjunto com o modulo Tests localizado em ws-tests/app/modules/tests.

3) Rotas para requisições via postman: http://localhost/ws-tests/pizzas-uds/ para verbos HTTP POST, GET e PUT. Como foi solicitado somente consulta, adição e alteração do pedido via api, não foi adicionado solicitações de exclusão de pedidos com verbo HTTP DELETE.

4) 	A implementação do webservice propriamente dito junto aos requisitos 1,2 e 3 solicitados no execício pizzaria uds localiza-se em ws-tests/app/modules/pizzas
	Vide: https://drive.google.com/file/d/1uFfEwqT7BSrWxX_5lQz2JofHM227HXL4/view

Ex POST: 
variavel: tamanho, valor: pequena
variavel: sabor, valor: marguerita

Ex POST 2: 
variavel: tamanho, valor: media
variavel: sabor, valor: portuguesa
variavel: personalizacao, valor: borda_recheada

para mais exemplos veja o arquivo Testlib.php!

4) Caso queiram verificar a biblioteca Testslib em funcionamento as rotas abaixo estão disponíveis:

http://localhost/ws-tests/simulacao/get-pedido/[NUM_PEDIDO] => busca um pedido no ws;
http://localhost/ws-tests/simulacao/get-pedidos => busca todos os pedidos no ws;
http://localhost/ws-tests/simulacao/post-pedido-comum => insere um pedido comum (sem personalizacao) no ws;
http://localhost/ws-tests/simulacao/post-pedido-personalizado => insere um pedido com personalizacao no ws;
http://localhost/ws-tests/simulacao/put-pedido/[NUM_PEDIDO] => altera um pedido já feito no ws caso o cliente queira mudar a pizza;

Mais informações por favor consultar o arquivo ws-tests/app/config/routes.php

Tempo estimado para implementação: 4:00h;
Tempo efetivo: 3:45h;




