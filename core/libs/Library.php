<?php
class Library{

	protected $ci;

	public function __construct()
	{
		$this->ci =& get_instance();
		$this->getCi()->load->model('base/base_model');
	}

	public function getCi()
	{
		return $this->ci;
	}
}