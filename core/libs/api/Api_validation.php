<?php
class Api_validation extends Library{

	private $tamanhos;
	private $sabores;
	private $personalizacoes;

	const T_PEQUENA = 'pequena';
	const T_MEDIA = 'media';
	const T_GRANDE = 'grande';

	const S_CALABRESA = 'calabresa';
	const S_MARGUERITA = 'marguerita';
	const S_PORTUGUESA = 'portuguesa';

	const P_EXTRA_BACON = 'extra_bacon';
	const P_SEM_CEBOLA = 'sem_cebola';
	const P_BORDA_RECHEADA = 'borda_recheada';

	public function __construct()
	{
		parent::__construct();
		$this->setTamanhos(array(self::T_PEQUENA, self::T_MEDIA, self::T_GRANDE));
		$this->setSabores(array(self::S_CALABRESA, self::S_MARGUERITA, self::S_PORTUGUESA));
		$this->setPersonalizacoes(array(self::P_EXTRA_BACON, self::P_SEM_CEBOLA, self::P_BORDA_RECHEADA));
	}

	function isInt($number)
	{
		return preg_match("/^-?[0-9]+$/", $number);
	}

	function isCurrency($number)
	{
		$arrayCheck = explode('.', $number);
		if (count($arrayCheck) == 2)
		{
			return is_numeric($arrayCheck[0]) && is_numeric($arrayCheck[1][0]) && is_numeric($arrayCheck[1][1]) && (strlen($arrayCheck[1]) == 2);
		}
		return FALSE;	
	}

	function isDecimal($number)
	{
		return $this->isCurrency($number);
	}

    function validateTamanhos($tamanho)
    {
    	return in_array($tamanho, $this->getTamanhos());
    }

    function validateSabores($sabor)
    {
    	return in_array($sabor, $this->getSabores());
    }

    function validatePersonalizacoes($personalizacao)
    {
    	return in_array($personalizacao, $this->getPersonalizacoes());
    }

    public function getTamanhos()
    {
        return $this->tamanhos;
    }

    public function setTamanhos($tamanhos)
    {
        $this->tamanhos = $tamanhos;
    }

    public function getSabores()
    {
        return $this->sabores;
    }

    public function setSabores($sabores)
    {
        $this->sabores = $sabores;
    }

    public function getPersonalizacoes()
    {
        return $this->personalizacoes;
    }

    public function setPersonalizacoes($personalizacoes)
    {
        $this->personalizacoes = $personalizacoes;
    }
}